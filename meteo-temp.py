import minimalmodbus
import serial.tools.list_ports
import time

COM           = "/dev/ttyAMA0"
slave_address = 0x1

MODBUS_READ_INPUT = 0x4

REG_INPUT_TEMP = 0
REG_INPUT_DEW  = 1
REG_INPUT_HUMI = 4

minimalmodbus.BAUDRATE = 19200
minimalmodbus.TIMEOUT  = 5
minimalmodbus.PARITY   = 'N'

mb = minimalmodbus.Instrument(COM, slave_address, mode='rtu')

if not mb.serial.is_open:
  mb.serial.open()

while(1):
  raw_temp = mb.read_register(REG_INPUT_TEMP, functioncode=MODBUS_READ_INPUT)
  raw_humi = mb.read_register(REG_INPUT_HUMI, functioncode=MODBUS_READ_INPUT)
  raw_dew  = mb.read_register(REG_INPUT_DEW , functioncode=MODBUS_READ_INPUT)
  
  print('T: {0} H: {1} D: {2}'.format(raw_temp, raw_humi, raw_dew))

  time.sleep(1)
