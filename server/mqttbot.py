#!/usr/env/bin python
# -*- coding: utf-8 -*-

import sys
from telegram import ChatAction, ReplyKeyboardMarkup, ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import configparser
from datetime import datetime
import os

import syslog
import time
import struct

import requests

import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe

w_data = {'time': None, 'modbus/Meteo_Temp/state/temp': None, 'modbus/Meteo_Temp/state/humi': None, 'modbus/Meteo_Temp/state/dew_point': None} 

config  = configparser.ConfigParser()
currDir = os.path.dirname(os.path.realpath(__file__))
config.read(currDir + '/config.cfg')
token = config.get('BOT', 'token')
hostname = config.get('MQTT', 'hostname')
username = config.get('MQTT', 'username')
password = config.get('MQTT', 'password')

print(config.get('ME', 'my_ID')  )
    
def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val 


def about(bot, update):
    global about_users
    custom_keyboard = [["/data"],["/kitty"],["/about"]]
    reply_markup = ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    msg = "Weather station bot. \U0001F324 [shishoit.io](shishoit.io) " \
           "\n Data from [Svetlanovskaya Ploshchad'](https://goo.gl/maps/8jh3UnWstx6K1UHGA)  \U0001F5FA" \
           "\n /data — current weather conditions \n /kitty — random kitty photo"
    if update.message.chat_id != (int)(config.get('ME', 'my_ID')): 
        about_users = about_users + 1  
    
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg,parse_mode=ParseMode.MARKDOWN, reply_markup=reply_markup)


def tell_users(bot, update):
    global data_users
    global kitty_users
    global about_users
    
    msg = """
           User counter: \
           \ndata users:  *%d*  \
           \nkitty users: *%d* \
           \nabout users: *%d* \
           
                      
           """ % (data_users,
                  kitty_users,
                  about_users)
    
    bot.send_message(chat_id=update.message.chat_id,text=msg,parse_mode=ParseMode.MARKDOWN)
    print(update.message.chat_id, update.message.text)
    print('lol'  )


def tell_weather(bot, update):
    global data_users
    dew_point = twos_comp(w_data['modbus/Meteo_Temp/state/dew_point'], 16)
    temp = twos_comp(w_data['modbus/Meteo_Temp/state/temp'], 16)

    msg = """
           Weather data %s: \
           \n\U0001F321 Temperature: *%.1f* °C \
           \n\U0001F4A7 Humidity: *%.1f* %% \
           \n\U0001F32B Dew point: *%.1f* °C \
                      
           """ % (w_data['time'],
                  temp / 100.0,
                  w_data['modbus/Meteo_Temp/state/humi'] / 100.0,
                  dew_point / 100.0)

    bot.send_message(chat_id=update.message.chat_id, text=msg,parse_mode=ParseMode.MARKDOWN)
    
    if update.message.chat_id != (int)(config.get('ME', 'my_ID')): 
        data_users = data_users + 1  
    
    print(update.message.chat_id, update.message.text)


def send_kitty(bot, update):
    global kitty_users
    random_kitty = requests.get("http://thecatapi.com/api/images/get?format=src")
    bot.sendChatAction(chat_id=update.message.chat_id, action=ChatAction.TYPING)
    bot.sendPhoto(chat_id=update.message.chat_id, photo=random_kitty.url)
    if update.message.chat_id != (int)(config.get('ME', 'my_ID')):
        kitty_users = kitty_users + 1     

def print_mqqt_data(client, userdata, message):
    #global users
    w_data['time'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    w_data[message.topic] = int(message.payload.decode("utf-8"))
    
    print('topic: {0}\npayload: {1}'.format(message.topic, message.payload.decode("utf-8")))

def main():
    updater = Updater(config.get('BOT', 'token'))
    global data_users
    global kitty_users   
    global about_users 
    data_users = 0
    kitty_users = 0
    about_users = 0
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("about", about))
    dp.add_handler(CommandHandler("users", tell_users))
    dp.add_handler(CommandHandler("start", about))
    dp.add_handler(CommandHandler("data", tell_weather))
    dp.add_handler(CommandHandler("kitty", send_kitty))
    
    # log all errors
    # dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    hostname = config.get('MQTT', 'hostname')    
    username = config.get('MQTT', 'username')    
    password = config.get('MQTT', 'password')



    subscribe.callback(print_mqqt_data, 'modbus/Meteo_Temp/state/+', qos=0, userdata=None, hostname=hostname,
    port=1883, client_id="", keepalive=60, will=None, auth={'username':username, 'password':password}, tls=None,
    protocol=mqtt.MQTTv311)

if __name__ == '__main__':
    main()
