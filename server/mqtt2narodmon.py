#!/usr/env/bin python
# -*- coding: utf-8 -*-

import sys
import threading
import configparser
import requests
import socket
import os
import datetime as dt
import syslog
import time
import struct

import requests

import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import paho.mqtt.publish as publish

global t
t = dt.datetime.now()

w_data = {'time': None, 'modbus/Meteo_Temp/state/temp': None, 'modbus/Meteo_Temp/state/humi': None, 'modbus/Meteo_Temp/state/dew_point': None} 

config  = configparser.ConfigParser()
currDir = os.path.dirname(os.path.realpath(__file__))
config.read(currDir + '/config.cfg')
token = config.get('BOT', 'token')
hostname = config.get('MQTT', 'hostname')
username = config.get('MQTT', 'username')
password = config.get('MQTT', 'password')

print(config.get('ME', 'my_ID')  )
    
def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val 







def tell_weather(bot, update):
    global data_users
    dew_point = twos_comp(w_data['modbus/Meteo_Temp/state/dew_point'], 16)
    temp = twos_comp(w_data['modbus/Meteo_Temp/state/temp'], 16)

    msg = """
           Weather data %s: \
           \n\U0001F321 Temperature: *%.1f* °C \
           \n\U0001F4A7 Humidity: *%.1f* %% \
           \n\U0001F32B Dew point: *%.1f* °C \
                      
           """ % (w_data['time'],
                  temp / 100.0,
                  w_data['modbus/Meteo_Temp/state/humi'] / 100.0,
                  dew_point / 100.0)

    bot.send_message(chat_id=update.message.chat_id, text=msg,parse_mode=ParseMode.MARKDOWN)
    
    if update.message.chat_id != (int)(config.get('ME', 'my_ID')): 
        data_users = data_users + 1  
    
    print(update.message.chat_id, update.message.text)




def print_mqqt_data(client, userdata, message):
    #global users
    
    w_data['time'] = dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    w_data[message.topic] = int(message.payload.decode("utf-8"))

    #print('topic: {0}\npayload: {1}'.format(message.topic, message.payload.decode("utf-8")))

def subs():
    hostname = config.get('MQTT', 'hostname')
    username = config.get('MQTT', 'username')
    password = config.get('MQTT', 'password')
    subscribe.callback(print_mqqt_data, 'modbus/Meteo_Temp/state/+', qos=0, userdata=None, hostname=hostname,
    port=1883, client_id="", keepalive=60, will=None, auth={'username':username, 'password':password}, tls=None,
    protocol=mqtt.MQTTv311)

def posts():
  t = dt.datetime.now()
  while True:
      delta = dt.datetime.now()-t
      if delta.seconds >= 300:
            print("1 Min")
            # Update 't' variable to new time
            t = dt.datetime.now()
            tempNM = twos_comp(w_data['modbus/Meteo_Temp/state/temp'], 16)
            tempNM = tempNM / 100.0
            humiNM = w_data['modbus/Meteo_Temp/state/humi'] / 100.0
            hostnameNM = config.get('MQTTnm', 'hostname')
            usernameNM = config.get('MQTTnm', 'username')
            passwordNM = config.get('MQTTnm', 'password')
            ClientID = config.get('MQTTnm', 'ClientID')
            # MAC адрес прибора. Заменить на свой!
            DEVICE_MAC = ClientID # MAC dz Raspberry Pi

            # идентификатор прибора
            SENSOR_ID_1 = 'temp'
            SENSOR_ID_2 = 'humi'

            # значения датчиков, тип float/integer
            sensor_value_1 = tempNM
            sensor_value_2 = humiNM

            # создание сокета
            sock = socket.socket()

            # обработчик исключений
            try:
            # подключаемся к сокету
                sock.connect(('narodmon.ru', 8283))

            # формируем строку для сокета при единичном значении датчика
            #s_sock = ("#{}\n#{}#{}\n##".format(DEVICE_MAC, SENSOR_ID_1, sensor_value_1))

            # формируем строку для сокета при множественном значение датчиков
                s_sock = ("#{}\n#{}#{}\n#{}#{}\n##".format(DEVICE_MAC, SENSOR_ID_1, sensor_value_1, SENSOR_ID_2, sensor_value_2))

            # Пишем строку с консоль для контроля
            # эти три строки после отладки закомментировать
                print('======================================')
                print(s_sock)
                print('======================================')

            # Пишем в сокет
                sock.send(s_sock.encode('utf8'))

            # читаем ответ
                data = sock.recv(1024)
                sock.close()
            except socket.error as e:
                print('ERROR! Exception {}'.format(e))

            #mqtt.connect("dimonfofr/Meteo_Temp/status", "online",hostname=hostnameNM, port=1883, auth={'username':usernameNM, 'password':passwordNM},client_id=ClientID, keepalive=60, bind_address="")

            #publish.single("dimonfofr/Meteo_Temp/Temp", tempNM, port=1883, client_id=ClientID, auth={'username':usernameNM, 'password':passwordNM}, hostname=hostnameNM)



t = threading.Thread(name='subs', target=subs)
w = threading.Thread(name='posts', target=posts)



w.start()

t.start()


